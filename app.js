const express = require("express");

const app = express();
const port = 9999;

app.get("/", (req, res) => {
  res.send("This is application for CICD");
});

app.listen(port, () => {
  console.log(`Application run at http://localhost:${port}`);
});
